# Introduction

Cet exemple a pour but d'aider à la mise en place de postes de travail Ubuntu
20.04 dans un environnement Active Directory. Il est spécifiquement destiné aux
lycées agricoles publiques de la région Auvergne-Rhône-Alpes ayant migré sur
la « solution cible Atos », dans le sens où le système de déploiement nécessite
le minimun de modifications sur cette solution cible.

__Avertissement__ : les documents fournis ici le sont sans __aucune garantie__
de fonctionnement ni de sécurité. Bien que fonctionnels, les postes installés
n'ont été que très peu testés. Des erreurs sont certainement présentes,
n'hésitez pas à proposer des corrections.

L'installation des postes est faite de manière semi-automatisée via un serveur
PXE, qui servira également de cache APT. La configuration de ce serveur et des
postes de travail se fera avec le logiciel Ansible.


## Préparation générale

Sur un poste sous Linux, installer Ansible (les tests ont ici étés effectués
avec Ansible 2.9 mais des versions antérieures ne devraient pas poser de
problème). Ensuite, adapter le fichier `host.ini` selon les paramètres de votre
réseau.

Le playbook `ubuntu-20.04.yml` nécessite le module `cups_lpadmin` qui n'est pas
encore un module standard d'Ansible. Vous pouvez l'installer simplement pour
votre utilisateur Linux avec les commandes suivantes :

```sh
mkdir -p ~/.ansible/plugins/modules && cd ~/.ansible/plugins/modules/
wget https://github.com/k0ste/ansible/raw/cups_lpadmin/lib/ansible/modules/system/cups_lpadmin.py
```

Ce playbook nécessite certains fichiers, à placer dans répertoire nommé
`fichiers/`. Celui-ci contiendra :

- le fond d'écran : `fond-lycée.png`
- LibreOffice et son interface traduite, pour avoir une version récente et
  identique aux postes Windows. À télécharger depuis le site officiels, par
  exemple avec les commandes
  `wget https://download.documentfoundation.org/libreoffice/stable/6.3.4/deb/x86_64/LibreOffice_6.3.4_Linux_x86-64_deb.tar.gz`
  et `wget https://download.documentfoundation.org/libreoffice/stable/6.3.4/deb/x86_64/LibreOffice_6.3.4_Linux_x86-64_deb_langpack_fr.tar.gz`
- L'extension Grammalecte pour LibreOffice, à télécharger depuis
  https://grammalecte.net


## Serveur

Le fichier `serveur.yml` permet de configurer un serveur fournissant quelques
services :
- cache APT pour éviter que tous les postes téléchargent les paquets depuis
  Internet (apt-cacher-ng)
- démarrage de l'installateur Debian par le réseau (iPXE et
  di-netboot-assistant)
- automatisation des réponses pour l'installateur Ubuntu (preseed)

N'importe quel vieux poste avec carte Gigabit, 1Go de RAM et 50Go de disque
sera suffisant. Il n'y aura pas besoin d'en faire des sauvegardes. Les Dell
Optiplex 380 conviennent par exemple très bien à cet usage, et le BIOS permet
notamment d'allumer le serveur le matin (et donc d'économiser de l'électricité
la nuit).


### Installation

Le serveur doit être connecté sur le réseau pédagogique (avec proxy).

Si vous comptez déployer les postes via PXE, le serveur doit toujours avoir la
même adresse IP : la solution choisie ici est de faire une réservation
d'adresse sur le serveur DHCP.

Installer ensuite le serveur via l'ISO netinstall amd64 de Debian 10, de la
manière suivante :
- configuration réseau via DHCP
- entrer le FQDN de la machine (à adapter selon votre domaine) :
  `srv-postes-linux.TAPU006.etab.local`
- la récupération de l'heure via NTP peut échouer, ce n'est pas grave
- partitionnement automatique "tout dans une seule partition"
- pas de mot de passe root (le compte sera donc désactivé)
- un compte utilisateur nommé `administrateur` avec un mot de passe de votre
  choix
- utilisation du miroir APT par défaut
- si vous passer par un proxy, l'indiquer comme serveur mandataire HTTP
- ne pas choisir de participer à l'étude des paquets installés (il faudrait
  configuré l'envoi de mail depuis ce serveur pour que cela fonctionne)
- choix des paquets à installer :
  - utilitaires usuels du système
  - serveur SSH
- installer Grub sur le disque dur


### Configuration

Éventuellement configurer le BIOS du serveur pour qu'il s'allume tout seul le
matin, ce qui permettra de l'éteindre la nuit. Adapter le fichier `hosts.ini`
en conséquence.

Depuis votre poste sur lequel Ansible est installé, initier une première
connexion SSH simplement pour accepter la clé publique. Se placer ensuite dans
le répertoire contenant les fichiers et lancer le playbook :

```
ansible-playbook -i hosts.ini -kK serveur.yml
```

Taper le mot de passe SSH du serveur comme demandé.


### Installation des postes par le réseau

Pour démarrer l'installateur par le réseau sans avoir à toucher le serveur
DHCP, on utilise une image ISO personnalisée de iPXE : celle-ci est disponible
sur le serveur nouvellement configuré dans
`/home/administrateur/ipxe/src/bin/ipxe.usb`.

Il suffit de la mettre sur une clé USB. Si la clé est reconnue comme
`/dev/sdb` :

```sh
sudo dd if=/home/administrateur/ipxe/src/bin/ipxe.usb of=/dev/sdb
```


## Mettre en service un poste de travail


### Installation par le réseau

Ce chapitre vous concerne si vous avez installé un serveur comme indiqué
ci-dessus.

S'assurer que le serveur peut être joignable par les postes de travail sur les
ports suivants :

- TFTP (UDP 69)
- HTTP (TCP 80)

De plus, autoriser le serveur à envoyer des paquets UDP vers les postes de
travail (numéros de port aléatoire).

Attention : le disque dur de la machine sera effacé sans confirmation.

Démarrer le poste de travail sur cette clé. Lorsque le menu de démarrage du
serveur PXE s'affiche, enlever la clé USB puis choisir l'option de démarrage
"Focal + preseed". Seules deux questions vous seront posées :

- le nom du poste à installer (sans le nom de domaine)
- le mot de passe à utiliser pour le compte `administrateur` du poste


### Configuration

Depuis votre poste Linux sur lequel Ansible est installé, initier une première
connexion SSH simplement pour accepter la clé publique. Se placer ensuite dans
le répertoire contenant les fichiers et lancer le playbook :

```sh
ansible-playbook -i hosts.ini -kK ubuntu-20.04.yml
```

Si vous configurer plusieurs postes en même temps, ne pas hésiter à utiliser
l'option `--forks` ou `-f` pour accélérer les choses. Pour exécuter le playbook
seulement sur certains postes, utiliser l'option `--limit` ou `-l`.


## Licence et auteur

Les fichiers de ce dépôts ont été créés en 2020 par Yvan Masson et sont placés
sous licence GPLv3.
